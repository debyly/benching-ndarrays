let {Nd} = require("@dably/wasm-ndarray")
Nd.init()
function initArray(nx,ny,nz){
  var sz = nx * ny * nz;
  return Nd.zeros([sz]);
}
function benchmark(A, B, nx, ny, nz, num_iter) {
  var sz = nx * ny * nz;
  for(var count=0; count<num_iter; ++count) {
    for(var i=sz-1; i>=0; --i) {
      //Ai += Bi + 0.1;
        A.set(i,A.get(i) + B.get(i) + 1.0)
        B.set(i, B.get(i) - A.get(i)*5.0)
    }
  }
}

exports.benchmark = benchmark;
exports.initArray = initArray;
exports.prop_name = "wasm-ndarray outside module ops";
