// runs operation within the wasm-ndarray instead
let {Nd} = require("@dably/wasm-ndarray")
Nd.init()
function initArray(nx,ny,nz){
  var sz = nx * ny * nz;
  return Nd.zeros([sz]);
}
function benchmark(A, B, nx, ny, nz, num_iter) {
  var sz = nx * ny * nz;
  for(var count=0; count<num_iter; ++count) {
      // run op
      Nd.run_bench_op(sz) // runs equivalent process within wasm-module
  }
}

exports.benchmark = benchmark;
exports.initArray = initArray;
exports.prop_name = "wasm-ndarray inside module bench";
