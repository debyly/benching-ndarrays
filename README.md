# Purpose 

This repo ideally will serve as a way to compare performance between the wasm-ndarray and the scijs-ndarray linear algebra libraries.


Presently, the repository is to share the strange results of how much faster scijs is than numpy or even rust's bluss/ndarray (see rust_perf branch). 

**The question put to the reader here is "are there any obvious ways to close this gap?"**

---
# To Run

```
npm install
node experiment.js
```
